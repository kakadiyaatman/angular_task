import { Injectable } from '@angular/core';
// import productData from './productData.json';  
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// interface product {  
//     id: Number;  
//     name: String;  
//     email: String;  
//     gender: String;  
// }  

@Injectable({
  providedIn: 'root',
})
export class ProductService {

    constructor(private http: HttpClient) {
        // this.getJSON().subscribe(data => {
        //     // console.log("from service",data);
        // });
    }
    public getJSON(): Observable<any> {
        return this.http.get("./assets/productData.json");
    }
  
}
