import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShowProductComponent} from './show-product.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
const routes = [
  {
    path: '',
    component: ShowProductComponent,
  },
];
@NgModule({
  declarations: [ShowProductComponent,HeaderComponent],
  imports: [
    CommonModule,
    CarouselModule,
    RouterModule.forChild(routes),
  ],
  exports: []
})
export class ShowProductModule { }
