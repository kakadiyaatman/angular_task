import { Component, Input, OnInit } from '@angular/core';
import {cartService} from '../././../assets/shared.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  clickEventsubscription:Subscription;
  data:any;
  constructor(private cartService:cartService) {
    this.clickEventsubscription= this.cartService.getOption().subscribe((response)=>{
      this.data= response;
      console.log(this.data);
      })
   }

  ngOnInit(): void {
    console.log(this.data);
  }
  onBookAdded(eventData: { title: string }) {
    // this.favBooks = this.favBooks.concat({
    //   title: eventData.title,
    // });
  }
  addToCart(event:any)
{
  console.log("from child",event)
}


}
