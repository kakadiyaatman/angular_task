import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import {ShowProductModule} from './show-product/show-product.module';
import { HttpClientModule } from '@angular/common/http';
// import {ProductHeaderModule} from './product-header/product-header.module';
import { FooterComponent } from './footer/footer.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
const appRoutes: Routes = [
  {
    path: 'showProduct',
    loadChildren: () => import('./show-product/show-product.module').then(m => m.ShowProductModule)
  },

  {
    path: '',
    redirectTo: '/showProduct',
    pathMatch: 'full'
  },

];
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ShowProductModule,
    // ProductHeaderModule,
    BrowserAnimationsModule,
	ToastrModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
