import { Injectable } from '@angular/core';  
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
  })
export class cartService {  
 

    constructor() { }
    public count = 0;
    private subject = new Subject<any>();
    setOption() {
    this.count = this.count +1;
    this.subject.next(this.count);
  }
  getOption(): Observable<any>{ 
    return this.subject.asObservable();
  }
}  